﻿using System;

namespace SHS.DateTimeHelper
{
    public class FormatTransition
    {
        /// <summary>
        /// 获取时间差
        /// </summary>
        /// <param name="startDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <returns></returns>
        public static TimeSpan GetDifferDate(DateTime startDate, DateTime endDate)
        {
            var result = new TimeSpan();
            if (endDate > startDate)
            {
                result = endDate - startDate;
            }
            return result;
        }

        /// <summary>
        /// 将时间转为mm:ss.fff
        /// </summary>
        /// <param name="timeSpan">时间差</param>
        /// <returns></returns>
        public static string GetStrDateToMinutesAndMinSec(TimeSpan timeSpan)
        {
            var time = timeSpan.TotalMilliseconds;
            var min = Math.Floor(time / 60000);
            time = time - (min * 60000);
            var sec = Math.Floor(time / 1000);
            time = time - (sec * 1000);
            var millSec = time;
            return string.Format("{0}:{1}.{2}", min, sec, millSec);
        }
    }
}
