using SHS.DateTimeHelper;
using System;
using Xunit;

namespace SHS.Xunit.Test
{
    public class DateTimeHelperTest
    {
        [Fact]
        public void Test1()
        {
            DateTime startDate = new DateTime();
            DateTime endDate = startDate.AddSeconds(10);
            var timeSpan = FormatTransition.GetDifferDate(startDate, endDate).TotalSeconds;
            Assert.Equal(10, int.Parse(timeSpan.ToString()));
        }
    }
}
